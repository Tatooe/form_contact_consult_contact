<?php
declare(strict_types = 1);


if (empty($_POST)) :
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire de contact BDD</title>
    <link rel="stylesheet" href="./style/style.css">
</head>


<header>
<h1 id="title_form"> FORMULAIRE BDD </h1>
</header>


<body>
    
<form class="container_form" action="indexBDD.php" method="post"></br>
<div class="form">
<label class="label"> Prénom:</label>
<input class="input" type="text" name="prenom" minlength="2" placeholder="Prénom"/></br>

<label class="label"> Nom*:</label>
<input class="input" type="text" name="nom" minlength="2" placeholder="Nom (Obligatoire)" required/></br>

<label class="label"> Téléphone:</label>
<input class="input" type="text" name="tel" minlength="10" placeholder="xx-xx-xx-xx-xx ou xxxxxxxxxx" pattern="[0]{​​1}​​[1-7]{​​1}​​[0-9]{​​8}"/></br>

<label class="label">Mail*:</label>
<input class="input" type="email" name="email" minlength="7" placeholder="xxxxx@xxx.xx (Obligatoire)" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required/></br>

<label class="label" for="msg">Message*: </label>
<textarea class="input" name="message" maxlength="400" placeholder="Entrer votre message (Obligatoire)" required></textarea></br>
</div>
<small>*Champs Obligatoires</small>

<input id="btn" type="submit" value="Envoyer" />
</form>

</body>

</html>

<?php

else:
    // Vérification de la présence des données obligatoires
    if (isset($_POST["email"]) && isset($_POST["message"]) && isset($_POST["nom"])) { 

        // Vérification de la presence de contenu dans les champs
        if(!empty($_POST["email"]) && !empty($_POST["message"] && !empty($_POST["nom"]))){
            

            // Vérification des valeurs remplies par l'utilisateur, sont elles conformes?
                // Pour le mail
            if(filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
                // On nettoie tout de même le reçu
                $_POST["email"]=filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
                
                if (filter_var($_POST["message"], FILTER_SANITIZE_SPECIAL_CHARS)) {
                    $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_SPECIAL_CHARS);

                    if(filter_var($_POST["nom"], FILTER_SANITIZE_SPECIAL_CHARS)) {
                        $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_SPECIAL_CHARS);

                        // Vérification des données non obligatoires "sanitize"
                            if(isset($_POST["prenom"])){
                                $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                            }else{
                                $_POST["prenom"] = "";
                            }

                            if (isset($_POST["telephone"])) {
                                $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_SPECIAL_CHARS);
                            } else {
                                $_POST["telephone"] = '';
                            }

                            // Si toutes les informations sont bonnes et/ou nettoyer

                            //On envoie les données à la BDD

                            require_once './SQL/ReqMng.php';
                            require_once './SQL/ServerParam.php';
                            
                            
                            function RequestManager(object $RequestToHandle = null, array $RequestParams = [], bool $IsSelectRequest = true){
                                
                                //Test de réponse de la BDD et gestion des erreurs
                                if (is_a($RequestToHandle, "PDOStatement")) {
                                    try{
                                        $RequestToHandle->execute($RequestParams);
                                        $Nbr = $RequestToHandle -> rowCount();
                                        if($Nbr > 0){
                                            if ($IsSelectRequest) {
                                                while ($DataLine = $RequestToHandle->fetch(PDO::FETCH_ASSOC)) {
                                                    print_r($DataLine);
                                                }
                                            }
                                            
                                        }
                                    }catch(Exception $ExceptionRaised){
                                        $RequestToHandle->errorInfo();
                                    }
                                }else{
                                    print "ERREUR !!! La variable passée en 1er argument de RequestManager() n'est pas une requète de type PDOStatement" . PHP_EOL . PHP_EOL;
                                }
                            }
                            
                            
                            
                            
                            (object) $MyDB = null;
                            
                            try{
                                //Connexion à la BDD avec ServerParam et gestion des erreurs en cas d'échec
                                $MyDB = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD, array(
                                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                                        ));
                            }catch(Exception $ExceptionRaised){
                                print "La connexion n'a pas pu se faire avec les identifiants par défaut." . PHP_EOL . PHP_EOL;
                                print("Problème d'enregistrement du message, Veuillez contacter votre administrateur réseau ou la personne en charge du site ");
                            }
                            
                            
                            $MyDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            
                            /**
                             * Préparation des requêtes
                             * */
                            $MyReqEmailAdd = $MyDB->prepare(REQ_EMAIL_ADD);
                            $MyReqPersonAdd = $MyDB->prepare(REQ_PERSON_ADD);
                            $MyReqMessageAdd = $MyDB->prepare(REQ_MESSAGE_ADD);
                            $MyReqPersAddIdConnu = $MyDB->prepare(REQ_PERSON_ADD_ID_CONNU);
                            $MyResMessageAddIdConnu = $MyDB->prepare(REQ_MESSAGE_ADD_ID_CONNU);
                            $MyReqSelectIdEmail = $MyDB->prepare(REQ_SELECT_IDMAIL);
                            /**
                             * Transaction pour ajout de l'email
                             * */
                            //Vérifie si une transaction est déjà en cours
                            if(!($MyDB->inTransaction())){
                                try{
                                    //Voir si l'adresse email existe déjà
                                    //Si oui, prendre l'id et enregistrer les données avec cet id
                                    //Si non, enregistrer cet nouvel email
                                    
                                    $MyDB->beginTransaction();
                                    
                                    $MyReqParams = [
                                        ":Email" => $_POST["email"],
                                    ];
                                    
                                    $MyReqSelectIdEmail->execute($MyReqParams);
                                    (array)$Dataline = $MyReqSelectIdEmail->fetch(PDO::FETCH_ASSOC);
                                    
                                    //Si j'ai un résultat et que le tableau $Dataline n'est pas vide
                                    if(!empty($Dataline)){
                                        
                                        //On récupère la valeur existante de dataline dans les paramètres
                                        
                                        $MyReqParams =[
                                        ":ValNom" => $_POST["nom"],
                                        ":ValPrenom"=>$_POST["prenom"],
                                        ":ValTel"=>$_POST["tel"],
                                        ":ValIdEmail"=>$Dataline["id_email"]
                                        ];
                                        //On lance la requête de rajout de personne
                                        RequestManager($MyReqPersAddIdConnu, $MyReqParams , false);
                                        
                                        
                                        $MyReqParams=[
                                            ":Message"=>$_POST["message"],
                                            ":ValIdEmail"=>$Dataline["id_email"]
                                        ];
                                        //On lance la requête d'ajout du message
                                        RequestManager($MyResMessageAddIdConnu, $MyReqParams , false);
                                        
                                        $MyDB->commit();
                                        
                                    }else{
                                        //Si l'email n'est pas connu
                                        $MyReqParams =[
                                        ":Email"=>$_POST["email"],
                                        ];
                                        //On lance la requête d'ajout d'email
                                        RequestManager($MyReqEmailAdd, $MyReqParams , false);
                                        //On récupère l'id créé
                                        $MyReqSelectIdEmail->execute($MyReqParams);
                                        (array)$EmailId = $MyReqSelectIdEmail->fetch(PDO::FETCH_ASSOC);
                                        
                                        //On utilise l'id du tableau $EmailId
                                        
                                        $MyReqParams =[
                                        ":ValNom" => $_POST["nom"],
                                        ":ValPrenom"=>$_POST["prenom"],
                                        ":ValTel"=>$_POST["tel"],
                                        ":ValIdEmail"=>$EmailId["id_email"],
                                        ];
                                        //On lance la requête d'ajout de personne sur l'id_email créé
                                        RequestManager($MyReqPersonAdd,$MyReqParams , false);
                                        
                                        $MyReqParams=[
                                            ":Message"=>$_POST["message"],
                                            ":ValIdEmail"=>$EmailId["id_email"],
                                        ];
                                        //On lance la requête d'ajout de message avec l'id créé
                                        RequestManager($MyReqMessageAdd, $MyReqParams , false);
                                        
                                        //On oublie pas de committer !!! :)
                                        $MyDB->commit();
                                        ?>
                                        <h1>Votre message a été envoyé avec succès</h1>
                                        <meta http-equiv='Refresh' content='3;URL=indexBDD.php'>
                                        <?php 
                                    }
                                    
                                } catch (Exception $ExceptionRaised) {
                                    $MyDB->rollBack();
                                    print_r($MyDB->errorInfo());
                                    // die= Affiche un message et termine le script courant (equivalent ÃƒÂ  exit)
                                    die("Error: " . $ExceptionRaised->getMessage());
                                }
                            } else {
                                ?><h2>
                                <?php 
                                print 'Impossible de commencer la transaction. Une autre transaction est déjà en cours. Veuillez essayer plus tard' . PHP_EOL . PHP_EOL;
                                ?>
                                </h2>
                                <meta http-equiv='Refresh' content='3;URL=indexBDD.php'>
                                <?php 
                            }
                            
                    

                    }else{
                        //Si les données reçues concernant le nom ne sont pas bonnes
                        ?>                        
                        <p>Merci de reformuler votre nom </p>
                        
                        <meta http-equiv='Refresh' content='3;URL=indexBDD.php'>
                        <?php 
                        
                    }
                
                }else{
                    // Si les données du message ne sont pas bonnes
                    ?>                        
                        <p>Merci de reformuler votre message </p>
                        
                        <meta http-equiv='Refresh' content='3;URL=indexBDD.php'>
                    <?php 
                }

            }else{
                // si les champs remplies dans l'email sont non conformes
                ?>
                        <p>Merci de reformuler votre email </p>
                        
                        <meta http-equiv='Refresh' content='3;URL=indexBDD.php'>
                    <?php 
            }

        }else{
            // Si les champs obligatoires sont vides
            ?>
                <p>Merci de remplir tous les champs obligatoires </p>
                
                <meta http-equiv='Refresh' content='3;URL=indexBDD.php'>
             <?php 
        }

    }else{
        // Si les données obligatoires ne sont pas présentes
        ?>
           <p>Merci de rafraichir et de ne pas toucher au html ^^</p>
           
           <meta http-equiv='Refresh' content='3;URL=indexBDD.php'>
        <?php 
    }
    
    endif;