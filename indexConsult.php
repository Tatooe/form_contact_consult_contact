<?php

declare(strict_types=1);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Interface consultation mail</title>
    <link rel="stylesheet" href="./style/style.css">
</head>



<?php
require_once './SQL/ReqMng.php';
require_once './SQL/ServerParam.php';


function RequestManager(object $RequestToHandle = null, array $RequestParams = [], bool $IsSelectRequest = true)
{

    //Test de réponse de la BDD et gestion des erreurs
    if (is_a($RequestToHandle, "PDOStatement")) {
        try {
            $RequestToHandle->execute($RequestParams);
            $Nbr = $RequestToHandle->rowCount();
            if ($Nbr > 0) {
                if ($IsSelectRequest) {
                    while ($DataLine = $RequestToHandle->fetch(PDO::FETCH_ASSOC)) {
                        print_r($DataLine);
                    }
                }
            }
        } catch (Exception $ExceptionRaised) {
            $RequestToHandle->errorInfo();
        }
    } else {
        print "ERREUR !!! La variable passée en 1er argument de RequestManager() n'est pas une requète de type PDOStatement" . PHP_EOL . PHP_EOL;
    }
}




(object) $MyDB = null;

try {
    //Connexion à la BDD avec ServerParam et gestion des erreurs en cas d'échec
    $MyDB = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD, array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
    ));
} catch (Exception $ExceptionRaised) {
    print "La connexion n'a pas pu se faire avec les identifiants par défaut." . PHP_EOL . PHP_EOL;
    print("Problème d'enregistrement du message, Veuillez contacter votre administrateur réseau ou la personne en charge du site ");
}


$MyDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$MyReqSelectAll = $MyDB->prepare(REQ_SELECT_ALL);
$MyReqSelectAll->execute();
(array) $Donnees = $MyReqSelectAll->fetchAll(PDO::FETCH_ASSOC);


?>
<header>
    <h1>Mes Mails de ma BDD</h1>
</header>

<section class="message_row">
    <table>

        <tr id="title_tbl">
            <th class="title_table">Nom</th>
            <th class="title_table">Prénom</th>
            <th class="title_table">Téléphone</th>
            <th class="title_table"> Adresse Mail </th>
            <th class="title_table"> Date</th>
            <th class="title_table">Message</th>
            <th class="title_table">Etat</th>
            <th class="title_table">Modifier</th>
            <th class="title_table">Supprimer</th>
        </tr>

        <?php for ($i = 0; $i < count($Donnees); $i++) {

        ?>

            <tr>
                <td class="donnees">
                    <?php
                    print(filter_var($Donnees[$i]["nom"], FILTER_SANITIZE_EMAIL));
                    ?>
                </td>
                                <td class="donnees">
                    <?php
                    print(filter_var($Donnees[$i]["prenom"], FILTER_SANITIZE_EMAIL));
                    ?>
                </td>
                                <td class="donnees">
                    <?php
                    print(filter_var($Donnees[$i]["tel"], FILTER_SANITIZE_EMAIL));
                    ?>
                </td>
                
                <td class="donnees">
                    <?php
                    print(filter_var($Donnees[$i]["email"], FILTER_SANITIZE_EMAIL));
                    ?>
                </td>

                <td class="donnees">
                    <?php
                    print(filter_var($Donnees[$i]["date"], FILTER_SANITIZE_STRING));
                    ?>
                </td>

                <td class="donnees">
                    <?php
                    print(filter_var($Donnees[$i]["message"], FILTER_SANITIZE_STRING));
                    ?>
                </td>

                <td class="donnees">
                    <form action="indexConsult.php" method="post">
                        <?php
                        print(filter_var($Donnees[$i]["etat"], FILTER_SANITIZE_STRING));
                        ?>
                </td>
                <td id="container_select">
                    <select name="etat">
                        
                        <option value=1>relancer</option>
                        <option value=2>en attente de réponse</option>
                        <option value=3>RDV pris</option>
                        <option value=4>sans suite</option>
                        <option value=5>attente de traitement</option>
                    </select>
                    <input type="hidden" name="IdMessage" value=<?php print($Donnees[$i]["id_message"]); ?>>
                    <input id="submit_select" type="submit" name="submit" value="Modifier">
                    </form>

                </td>
                <td class="table_delete">
                    <form action="indexConsult.php" method="post">
                        <input type="hidden" name="delete" value=<?php print($Donnees[$i]["id_message"]); ?>>
                        <input type="hidden" name="IdMessage" value=<?php print($Donnees[$i]["id_message"]); ?>>
                        <input class="trash_icon" type="image" name="btn_delete" alt="delete" src="./img/poubelle.png">
                    </form>
                </td>
                <?php
            }
            //on prépare la requête de mise à jour de l'état
            $MyReqUpdateEtat = $MyDB->prepare(REQ_UPDATE_ETAT);
            $MyReqDeleteAllLigne = $MyDB->prepare(REQ_DELETE_ALL_LIGNE);
            //Vérifie si une transaction est déjà en cours
            if (!($MyDB->inTransaction())) {
                try {


                    $MyDB->beginTransaction();

                    if (isset($_POST["IdMessage"]) && isset($_POST["delete"])) {
                        if (!empty($_POST["IdMessage"]) && !empty($_POST["delete"])) {

                            $IdMessage = filter_var($_POST['IdMessage'], FILTER_SANITIZE_NUMBER_INT);

                            $MyRequestParams = [
                                ":ValIDMessage" => $IdMessage,
                            ];

                            RequestManager($MyReqDeleteAllLigne, $MyRequestParams, false);
                            $MyDB->commit();

                ?>
                            <meta http-equiv='Refresh' content='1;URL=indexConsult.php'>
                            <?php

                        }
                    }







                    if (isset($_POST['etat']) && isset($_POST['IdMessage'])) {
                        if (!empty($_POST['etat']) && !empty($_POST['IdMessage'])) {

                            $Choix = filter_var($_POST['etat'], FILTER_SANITIZE_NUMBER_INT);
                            $IdMessage = filter_var($_POST['IdMessage'], FILTER_SANITIZE_STRING);


                            switch ($Choix) {
                                case "1":
                                    $MyRequestParams = [
                                        ":NewEtat" => "relancer",
                                        ":ValIDMessage" => $IdMessage
                                    ];

                                    RequestManager($MyReqUpdateEtat, $MyRequestParams, false);
                                    $MyDB->commit();
                            ?>
                                    <meta http-equiv='Refresh' content='1;URL=indexConsult.php'>
                                <?php
                                    break;


                                case "2":
                                    $MyRequestParams = [
                                        ":NewEtat" => "attente de reponse",
                                        ":ValIDMessage" => $IdMessage
                                    ];
                                    RequestManager($MyReqUpdateEtat, $MyRequestParams, false);
                                    $MyDB->commit();
                                ?>
                                    <meta http-equiv='Refresh' content='1;URL=indexConsult.php'>
                                <?php
                                    break;


                                case "3":
                                    $MyRequestParams = [
                                        ":NewEtat" => "RDV pris",
                                        ":ValIDMessage" => $IdMessage
                                    ];
                                    RequestManager($MyReqUpdateEtat, $MyRequestParams, false);
                                    $MyDB->commit();
                                ?>
                                    <meta http-equiv='Refresh' content='1;URL=indexConsult.php'>
                                <?php
                                    break;


                                case "4":
                                    $MyRequestParams = [
                                    ":NewEtat" => "sans suite",
                                    ":ValIDMessage" => $IdMessage
                                    ];
                                    RequestManager($MyReqUpdateEtat, $MyRequestParams, false);
                                    $MyDB->commit();
                                ?>
                                    <meta http-equiv='Refresh' content='1;URL=indexConsult.php'>
                                <?php
                                    break;


                                default:
                                    $MyRequestParams = [
                                        ":NewEtat" => "attente de traitement",
                                        ":ValIDMessage" => $IdMessage
                                    ];
                                    RequestManager($MyReqUpdateEtat, $MyRequestParams, false);
                                    $MyDB->commit();
                                ?>
                                    <meta http-equiv='Refresh' content='1;URL=indexConsult.php'>
                                <?php
                                    break;
                            }
                        }
                    }
                } catch (Exception $ExceptionRaised) {
                    $MyDB->rollBack();
                    print_r($MyDB->errorInfo());
                    // die= Affiche un message et termine le script courant (equivalent ÃƒÂ  exit)
                    die("Error: " . $ExceptionRaised->getMessage());
                }
            } else {
                ?><h2>
                    <?php
                    print 'Impossible de commencer la transaction. Une autre transaction est déjà en cours. Veuillez essayer plus tard' . PHP_EOL . PHP_EOL;
                    ?>
                </h2>
                <meta http-equiv='Refresh' content='3;URL=indexConsult.php'>
            <?php
            }

            ?>
            </td>
            </tr>
    </table>
</section>

</html>